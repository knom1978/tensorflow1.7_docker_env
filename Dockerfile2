FROM nvidia/cuda:9.2-cudnn7-runtime-ubuntu16.04

LABEL maintainer 'cecil_liu@umc.om'

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        software-properties-common \
        python3-pip \
 && add-apt-repository ppa:deadsnakes/ppa \
 && apt-get update \
 && apt-get install -y python3.6 python3.6-dev \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# pip has to be installed before setuptools, setuptools has to be installed before tensorflow
RUN python3.6 -m pip install --no-cache-dir -U pip
RUN python3.6 -m pip install --no-cache-dir -U setuptools
# also useful
RUN python3.6 -m pip install --no-cache-dir ipython requests numpy pandas quandl scipy
RUN python3.6 -m pip install --no-cache-dir tensorflow-gpu==1.7.1
RUN ln -s /usr/bin/python3.6 /usr/bin/python

##############################################################################
# opencv
##############################################################################
RUN apt-get update && \
        apt-get install -y \
        build-essential \
        cmake \
        git \
        wget \
        unzip \
        yasm \
        pkg-config \
        libswscale-dev \
        libtbb2 \
        libtbb-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libavformat-dev \
        libpq-dev
RUN pip3 install opencv-contrib-python
RUN pip3 install --no-cache-dir opencv-python

##############################################################################
# keras
##############################################################################
ARG KERAS_VERSION=2.1.4
ENV KERAS_BACKEND=tensorflow
RUN pip3 --no-cache-dir install git+https://github.com/fchollet/keras.git@${KERAS_VERSION}

RUN mkdir /home/ubuntu
WORKDIR /home/ubuntu

RUN ["/bin/bash"]