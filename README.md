# An introduction for using docker images as tensofrlow 1.7 script runtimes
This **Image/Dockerfile** aims to create a container for tensofrlow 1.7 runtimes.

## How to use?
* You should git clone this repository first.
* And then cd into the folder, type as follows according to your cuda version and torch version.
```
sudo docker build --no-cache -f Dockerfile -t tensorflow_env/tf1_7_1:cu92cudnn7_1604 .
sudo docker build --no-cache -f Dockerfile2 -t tensorflow_env/tf1_7_1:cu92cudnn7_1604_2 .
```
* For your tensorflow 1.7 scripts, you can use as the hereunder part.
If you ran your previous python code as 
```
python3 a.py
```
* Now you should run as :
```
sudo docker run -it --rm -v "$PWD":/home/ubuntu -w /home/ubuntu tensorflow_env/tf1_7_1:cu92cudnn7_1604 python3 a.py
sudo docker run -it --rm -v "$PWD":/home/ubuntu -w /home/ubuntu tensorflow_env/tf1_7_1:cu92cudnn7_1604_2 python3 a.py
```
